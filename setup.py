#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import os
import re
import sys
import setuptools

from setuptools import setup, find_packages, Extension, dist
from setuptools.command.build_ext import build_ext
from setuptools.command.test import test as TestCommand
from setuptools import Command


# http://fgimian.github.io/blog/2014/04/27/running-nose-tests-with-plugins-using-the-python-setuptools-test-command/
# Inspired by the example at https://pytest.org/latest/goodpractises.html
class NoseTestCommand(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        # Run nose ensuring that argv simulates running nosetests directly
        import nose
        nose.run_exit(argv=['nosetests'])

class GenerateLexicalParserCommand(Command):
    user_options = []

    def initialize_options(self):
        """Abstract method that is required to be overwritten"""

    def finalize_options(self):
        """Abstract method that is required to be overwritten"""

    def run(self):
        print(" => Generate lexical parser with wisent...")
        os.system("grammar\wisent\wisent.py  -o binmapper\parser\grammar.py grammar/binmapper.wi")
        
        
def read_file(filename):
    """
    Read a utf8 encoded text file and return its contents.
    """
    with codecs.open(filename, 'r', 'utf8') as f:
        return f.read()

def read_md_file(filename):
    """
    Read markdown file and return as RestructuredText.
    Return plaintext if has errors.
    """
        
    try:
        import pypandoc
        long_description = pypandoc.convert('README.md', 'rst')
        return long_description
    except ImportError:
        print("Cant import pypandoc.")
        return read_file('README.md')
    except IOError:
        print("Has IO errors while convert to RST.")
        return read_file('README.md')
    
    return None

class get_pybind_include(object):
    """Helper class to determine the pybind11 include path

    The purpose of this class is to postpone importing pybind11
    until it is actually installed, so that the ``get_include()``
    method can be invoked. """

    def __init__(self, user=False):
        self.user = user

    def __str__(self):
        import pybind11
        return pybind11.get_include(self.user)

class get_pyunitylib_include(object):
    """Helper class to determine the pybind11 include path

    The purpose of this class is to postpone importing pybind11
    until it is actually installed, so that the ``get_include()``
    method can be invoked. """

    def __init__(self, user=False):
        self.user = user

    def __str__(self):
        return "e:/code/zog/unity-unpacker-lib.git/unity_unpacker_lib"

ext_modules = [
    Extension(
        'pyunitylib',
        ['src/pyunitylib/pyunitylib.cpp'],
        include_dirs=[
            # Path to pybind11 headers
            get_pybind_include(),
            get_pybind_include(user=True),
			get_pyunitylib_include(),
			"e:/code/zog/unity-unpacker-lib.git/include",
			"e:/code/zog/unity-unpacker-lib.git",
			"e:/code/zog/unity-unpacker-lib.git/include/binpac"
        ],
		extra_objects=['unity_unpacker_lib.lib','memory_storage.lib','debug_log.lib','user32.lib'],
        extra_link_args=['-LIBPATH:e:/code/zog/pyunitylib.git/libs/Win32'],
        language='c++'
    ),
]

# As of Python 3.6, CCompiler has a `has_flag` method.
# cf http://bugs.python.org/issue26689
def has_flag(compiler, flagname):
    """Return a boolean indicating whether a flag name is supported on
    the specified compiler.
    """
    import tempfile
    with tempfile.NamedTemporaryFile('w', suffix='.cpp') as f:
        f.write('int main (int argc, char **argv) { return 0; }')
        try:
            compiler.compile([f.name], extra_postargs=[flagname])
        except setuptools.distutils.errors.CompileError:
            return False
    return True
	
def cpp_flag(compiler):
    """Return the -std=c++[11/14] compiler flag.

    The c++14 is prefered over c++11 (when it is available).
    """
    if has_flag(compiler, '-std=c++14'):
        return '-std=c++14'
    elif has_flag(compiler, '-std=c++11'):
        return '-std=c++11'
    else:
        raise RuntimeError('Unsupported compiler -- at least C++11 support '
                           'is needed!')

class BinaryDistribution(dist.Distribution):
    def is_pure(self):
        return False
		
class BuildExt(build_ext):
    """A custom build extension for adding compiler-specific options."""
    c_opts = {
        'msvc': ['/EHsc'],
        'unix': [],
    }

    if sys.platform == 'darwin':
        c_opts['unix'] += ['-stdlib=libc++', '-mmacosx-version-min=10.7']

    def build_extensions(self):
        ct = self.compiler.compiler_type
        opts = self.c_opts.get(ct, [])
        if ct == 'unix':
            opts.append('-DVERSION_INFO="%s"' % self.distribution.get_version())
            opts.append(cpp_flag(self.compiler))
            if has_flag(self.compiler, '-fvisibility=hidden'):
                opts.append('-fvisibility=hidden')
        elif ct == 'msvc':
            opts.append('/DVERSION_INFO=\\"%s\\"' % self.distribution.get_version())
        for ext in self.extensions:
            ext.extra_compile_args = opts
        build_ext.build_extensions(self)
		
setup(
    name     = 'pyunitylib',
    version  = '0.1a0',
    packages = find_packages(),
    install_requires=['pybind11>=1.7'],
    description  = 'A Python wrapper for the unity_unpack_library which allow parse and store unity3d assets',
    long_description = read_md_file('README.md'), 
    author       = 'RedSkotina',
    author_email = 'red.skotina@gmail.com',
    url          = 'https://bitbucket.org/RedSkotina/pyunitylib',
    download_url = 'https://bitbucket.org/RedSkotina/pyunitylib/downloads',
    license      = read_file('LICENSE.txt'),
    keywords     = ['unity3d','parser'],
    classifiers  = [
        'Intended Audience :: Developers',
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: Apache Software License',
        "Programming Language :: Python :: 3",
		"Programming Language :: Python :: 3.6",
        'Natural Language :: English',
        'Operating System :: Microsoft :: Windows',
		'Operating System :: Microsoft :: Windows :: Windows XP',
		'Operating System :: Microsoft :: Windows :: Windows 7',
		'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    test_suite='nose.collector',
    tests_require=['nose','coverage',],
    cmdclass={
		'build_ext': BuildExt,
        'test': NoseTestCommand,
        'generate': GenerateLexicalParserCommand},
    zip_safe = False,
    ext_modules=ext_modules,
	distclass=BinaryDistribution,
	include_package_data=True,  # use MANIFEST.in during install
    package_data = {
            'pyunitylib': [
			'unity_unpacker_lib.dll',
			'plugins/assets_v4.dll',
			'plugins/assets_v5.dll',
			'plugins/tex.dll',
			'plugins/uogm.dll'],
            }
    
)