#include <map>
#include <memory>
#include <stdexcept>
#include <functional>

// File: std/xutility.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // Storage
#include <../plugins/plugin.h> // UUL_Entry
#include <../plugins/plugin.h> // UUL_Entry::UUL_Entry
#include <Pluma/Host.hpp> // pluma::Host
#include <Pluma/Provider.hpp> // pluma::Provider
#include <Pluma/Provider.hpp> // pluma::Provider::Provider
#include <Pluma/Provider.hpp> // pluma::Provider::getVersion
#include <Pluma/Provider.hpp> // pluma::Provider::isCompatible
#include <Pluma/Provider.hpp> // pluma::Provider::operator=
#include <iterator> // std::iterator
#include <iterator> // std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, UUL_Entry *, UUL_Entry &>::iterator
#include <iterator> // std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, UUL_Entry *, UUL_Entry &>::operator=
#include <iterator> // std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, const UUL_Entry *, const UUL_Entry &>::iterator
#include <iterator> // std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, const UUL_Entry *, const UUL_Entry &>::operator=
#include <iterator> // std::iterator_traits
#include <list> // std::list
#include <memory> // std::allocator
#include <memory> // std::allocator<RE_Matcher *>::address
#include <memory> // std::allocator<RE_Matcher *>::allocate
#include <memory> // std::allocator<RE_Matcher *>::allocator
#include <memory> // std::allocator<RE_Matcher *>::max_size
#include <memory> // std::allocator<char16_t>::address
#include <memory> // std::allocator<char16_t>::allocate
#include <memory> // std::allocator<char16_t>::allocator
#include <memory> // std::allocator<char16_t>::deallocate
#include <memory> // std::allocator<char16_t>::max_size
#include <memory> // std::allocator<char32_t>::address
#include <memory> // std::allocator<char32_t>::allocate
#include <memory> // std::allocator<char32_t>::allocator
#include <memory> // std::allocator<char32_t>::deallocate
#include <memory> // std::allocator<char32_t>::max_size
#include <memory> // std::allocator<char>::address
#include <memory> // std::allocator<char>::allocate
#include <memory> // std::allocator<char>::allocator
#include <memory> // std::allocator<char>::deallocate
#include <memory> // std::allocator<char>::max_size
#include <memory> // std::allocator<pluma::Provider *>::address
#include <memory> // std::allocator<pluma::Provider *>::allocate
#include <memory> // std::allocator<pluma::Provider *>::allocator
#include <memory> // std::allocator<pluma::Provider *>::max_size
#include <memory> // std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> >::address
#include <memory> // std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> >::allocate
#include <memory> // std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> >::allocator
#include <memory> // std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> >::deallocate
#include <memory> // std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> >::max_size
#include <memory> // std::allocator<std::pair<const int, Storage *> >::address
#include <memory> // std::allocator<std::pair<const int, Storage *> >::allocate
#include <memory> // std::allocator<std::pair<const int, Storage *> >::allocator
#include <memory> // std::allocator<std::pair<const int, Storage *> >::max_size
#include <memory> // std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > >::address
#include <memory> // std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > >::allocate
#include <memory> // std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > >::allocator
#include <memory> // std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > >::max_size
#include <memory> // std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::address
#include <memory> // std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::allocate
#include <memory> // std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::allocator
#include <memory> // std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::max_size
#include <memory> // std::allocator<unsigned int>::address
#include <memory> // std::allocator<unsigned int>::allocate
#include <memory> // std::allocator<unsigned int>::allocator
#include <memory> // std::allocator<unsigned int>::deallocate
#include <memory> // std::allocator<unsigned int>::max_size
#include <memory> // std::allocator<void>::allocator
#include <memory> // std::allocator<wchar_t>::address
#include <memory> // std::allocator<wchar_t>::allocate
#include <memory> // std::allocator<wchar_t>::allocator
#include <memory> // std::allocator<wchar_t>::deallocate
#include <memory> // std::allocator<wchar_t>::max_size
#include <memory> // std::allocator_arg_t
#include <memory> // std::allocator_traits
#include <memory> // std::allocator_traits<std::allocator<RE_Matcher *> >::allocate
#include <memory> // std::allocator_traits<std::allocator<RE_Matcher *> >::max_size
#include <memory> // std::allocator_traits<std::allocator<RE_Matcher *> >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<char16_t> >::allocate
#include <memory> // std::allocator_traits<std::allocator<char16_t> >::deallocate
#include <memory> // std::allocator_traits<std::allocator<char16_t> >::max_size
#include <memory> // std::allocator_traits<std::allocator<char16_t> >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<char32_t> >::allocate
#include <memory> // std::allocator_traits<std::allocator<char32_t> >::deallocate
#include <memory> // std::allocator_traits<std::allocator<char32_t> >::max_size
#include <memory> // std::allocator_traits<std::allocator<char32_t> >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<char> >::allocate
#include <memory> // std::allocator_traits<std::allocator<char> >::deallocate
#include <memory> // std::allocator_traits<std::allocator<char> >::max_size
#include <memory> // std::allocator_traits<std::allocator<char> >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<pluma::Provider *> >::allocate
#include <memory> // std::allocator_traits<std::allocator<pluma::Provider *> >::max_size
#include <memory> // std::allocator_traits<std::allocator<pluma::Provider *> >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::allocate
#include <memory> // std::allocator_traits<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::deallocate
#include <memory> // std::allocator_traits<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::max_size
#include <memory> // std::allocator_traits<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<std::pair<const int, Storage *> > >::allocate
#include <memory> // std::allocator_traits<std::allocator<std::pair<const int, Storage *> > >::max_size
#include <memory> // std::allocator_traits<std::allocator<std::pair<const int, Storage *> > >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::allocate
#include <memory> // std::allocator_traits<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::max_size
#include <memory> // std::allocator_traits<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::allocate
#include <memory> // std::allocator_traits<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::max_size
#include <memory> // std::allocator_traits<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<unsigned int> >::allocate
#include <memory> // std::allocator_traits<std::allocator<unsigned int> >::deallocate
#include <memory> // std::allocator_traits<std::allocator<unsigned int> >::max_size
#include <memory> // std::allocator_traits<std::allocator<unsigned int> >::select_on_container_copy_construction
#include <memory> // std::allocator_traits<std::allocator<wchar_t> >::allocate
#include <memory> // std::allocator_traits<std::allocator<wchar_t> >::deallocate
#include <memory> // std::allocator_traits<std::allocator<wchar_t> >::max_size
#include <memory> // std::allocator_traits<std::allocator<wchar_t> >::select_on_container_copy_construction
#include <set> // std::set
#include <sstream> // __str__
#include <string> // std::basic_string
#include <string> // std::char_traits
#include <utility> // std::pair
#include <xatomic0.h> // std::_Get_atomic_count
#include <xatomic0.h> // std::_Init_atomic_counter
#include <xatomic0.h> // std::memory_order
#include <xmemory0> // std::_Alloc_allocate
#include <xmemory0> // std::_Alloc_max_size
#include <xmemory0> // std::_Alloc_select
#include <xmemory0> // std::_Allocate
#include <xmemory0> // std::_Deallocate
#include <xmemory0> // std::_Has_no_alloc_construct_tag
#include <xmemory0> // std::_Has_no_alloc_destroy_tag
#include <xmemory0> // std::_Is_default_allocator
#include <xmemory0> // std::_Is_simple_alloc
#include <xmemory0> // std::_Pocca
#include <xmemory0> // std::_Pocs
#include <xmemory0> // std::_Simple_types
#include <xmemory0> // std::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<RE_Matcher *> >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<RE_Matcher *> >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<RE_Matcher *> >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<RE_Matcher *> >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<RE_Matcher *> >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<RE_Matcher *> >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char16_t> >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char16_t> >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char16_t> >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char16_t> >::deallocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char16_t> >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char16_t> >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char16_t> >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char32_t> >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char32_t> >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char32_t> >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char32_t> >::deallocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char32_t> >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char32_t> >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char32_t> >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char> >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char> >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char> >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char> >::deallocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char> >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char> >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<char> >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<pluma::Provider *> >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<pluma::Provider *> >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<pluma::Provider *> >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<pluma::Provider *> >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<pluma::Provider *> >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<pluma::Provider *> >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::deallocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::_Tree_node<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, void *> > >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const int, Storage *> > >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const int, Storage *> > >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const int, Storage *> > >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const int, Storage *> > >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const int, Storage *> > >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const int, Storage *> > >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<std::pair<const std::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::list<pluma::Provider *, std::allocator<pluma::Provider *> > > > >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<unsigned int> >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<unsigned int> >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<unsigned int> >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<unsigned int> >::deallocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<unsigned int> >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<unsigned int> >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<unsigned int> >::select_on_container_copy_construction
#include <xmemory0> // std::_Wrap_alloc<std::allocator<wchar_t> >::_Wrap_alloc
#include <xmemory0> // std::_Wrap_alloc<std::allocator<wchar_t> >::address
#include <xmemory0> // std::_Wrap_alloc<std::allocator<wchar_t> >::allocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<wchar_t> >::deallocate
#include <xmemory0> // std::_Wrap_alloc<std::allocator<wchar_t> >::max_size
#include <xmemory0> // std::_Wrap_alloc<std::allocator<wchar_t> >::operator=
#include <xmemory0> // std::_Wrap_alloc<std::allocator<wchar_t> >::select_on_container_copy_construction
#include <xstddef> // std::less
#include <xtr1common> // std::_Nil
#include <xtr1common> // std::_Nil::_Nil
#include <xtr1common> // std::integral_constant
#include <xtr1common> // std::integral_constant<bool, false>::integral_constant
#include <xtr1common> // std::integral_constant<bool, false>::operator()
#include <xtr1common> // std::integral_constant<bool, true>::integral_constant
#include <xtr1common> // std::integral_constant<bool, true>::operator()
#include <xtree> // std::_Tree_node
#include <xutility> // std::_Container_base0
#include <xutility> // std::_Is_character
#include <xutility> // std::_Is_iterator
#include <xutility> // std::_Iterator012
#include <xutility> // std::_Iterator012<std::random_access_iterator_tag, char, int, const char *, const char &, std::_Iterator_base0>::_Iterator012
#include <xutility> // std::_Iterator012<std::random_access_iterator_tag, char16_t, int, const char16_t *, const char16_t &, std::_Iterator_base0>::_Iterator012
#include <xutility> // std::_Iterator012<std::random_access_iterator_tag, char32_t, int, const char32_t *, const char32_t &, std::_Iterator_base0>::_Iterator012
#include <xutility> // std::_Iterator012<std::random_access_iterator_tag, unsigned int, int, const unsigned int *, const unsigned int &, std::_Iterator_base0>::_Iterator012
#include <xutility> // std::_Iterator012<std::random_access_iterator_tag, wchar_t, int, const wchar_t *, const wchar_t &, std::_Iterator_base0>::_Iterator012
#include <xutility> // std::_Iterator_base0
#include <xutility> // std::_Iterator_base0::_Adopt
#include <xutility> // std::_Iterator_base0::_Getcont
#include <xutility> // std::_Iterator_base0::_Iterator_base0
#include <xutility> // std::_Iterator_base0::operator=
#include <xutility> // std::_Ptr_cat_helper
#include <xutility> // std::_Xbad_alloc
#include <xutility> // std::_Xinvalid_argument
#include <xutility> // std::_Xlength_error
#include <xutility> // std::_Xout_of_range
#include <xutility> // std::_Xoverflow_error
#include <xutility> // std::_Xruntime_error
#include <xutility> // std::_Yarn
#include <xutility> // std::_Yarn<char>::_C_str
#include <xutility> // std::_Yarn<char>::_Empty
#include <xutility> // std::_Yarn<char>::_Yarn
#include <xutility> // std::_Yarn<char>::c_str
#include <xutility> // std::_Yarn<char>::empty
#include <xutility> // std::_Yarn<char>::operator=
#include <xutility> // std::_Yarn<wchar_t>::_C_str
#include <xutility> // std::_Yarn<wchar_t>::_Empty
#include <xutility> // std::_Yarn<wchar_t>::_Yarn
#include <xutility> // std::_Yarn<wchar_t>::c_str
#include <xutility> // std::_Yarn<wchar_t>::empty
#include <xutility> // std::_Yarn<wchar_t>::operator=
#include <xutility> // std::bidirectional_iterator_tag
#include <xutility> // std::bidirectional_iterator_tag::bidirectional_iterator_tag
#include <xutility> // std::random_access_iterator_tag
#include <xutility> // std::random_access_iterator_tag::random_access_iterator_tag

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_std_xutility(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // std::iterator file:xutility line:563
		pybind11::class_<std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,UUL_Entry *,UUL_Entry &>, std::shared_ptr<std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,UUL_Entry *,UUL_Entry &>>> cl(M("std"), "iterator_std_bidirectional_iterator_tag_UUL_Entry_int_UUL_Entry_*_UUL_Entry_&_t", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());

		cl.def(pybind11::init<const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> &>(), pybind11::arg(""));

		cl.def("assign", (struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> & (std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,UUL_Entry *,UUL_Entry &>::*)(const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> &)) &std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, UUL_Entry *, UUL_Entry &>::operator=, "C++: std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, UUL_Entry *, UUL_Entry &>::operator=(const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> &) --> struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	{ // std::iterator file:xutility line:563
		pybind11::class_<std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,const UUL_Entry *,const UUL_Entry &>, std::shared_ptr<std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,const UUL_Entry *,const UUL_Entry &>>> cl(M("std"), "iterator_std_bidirectional_iterator_tag_UUL_Entry_int_const_UUL_Entry_*_const_UUL_Entry_&_t", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> &>(), pybind11::arg(""));

		cl.def(pybind11::init<>());

		cl.def("assign", (struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> & (std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,const UUL_Entry *,const UUL_Entry &>::*)(const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> &)) &std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, const UUL_Entry *, const UUL_Entry &>::operator=, "C++: std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, const UUL_Entry *, const UUL_Entry &>::operator=(const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> &) --> struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}


// File: unknown/unknown.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // UUL_Entry
#include <../plugins/plugin.h> // UUL_Entry::UUL_Entry
#include <initializer_list> // std::initializer_list
#include <memory> // std::allocator
#include <set> // std::set
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::operator=
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::set
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::swap
#include <sstream> // __str__
#include <xstddef> // std::less

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_unknown_unknown(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // tcl::basic_tree file: line:44
		pybind11::class_<tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>, tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>*> cl(M("tcl"), "basic_tree_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_std_set_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_tcl_tree_deref_less_UUL_Entry_std_less_UUL_Entry_std_allocator_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_t", "");
		pybind11::handle cl_type = cl;

		cl.def("get", (const struct UUL_Entry * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get() const --> const struct UUL_Entry *", pybind11::return_value_policy::automatic);
		cl.def("get", (struct UUL_Entry * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get() --> struct UUL_Entry *", pybind11::return_value_policy::automatic);
		cl.def("is_root", (bool (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::is_root, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::is_root() const --> bool");
		cl.def("size", (unsigned int (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::size, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::size() const --> unsigned int");
		cl.def("max_size", (unsigned int (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::max_size, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::max_size() const --> unsigned int");
		cl.def("empty", (bool (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::empty, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::empty() const --> bool");
		cl.def("parent", (class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent() --> class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic);
		cl.def("parent", (const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent() const --> const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic);
	}
}


// File: vcruntime_typeinfo.cpp
#include <sstream> // __str__
#include <vcruntime_typeinfo.h> // type_info
#include <vcruntime_typeinfo.h> // type_info::before
#include <vcruntime_typeinfo.h> // type_info::hash_code
#include <vcruntime_typeinfo.h> // type_info::name
#include <vcruntime_typeinfo.h> // type_info::operator!=
#include <vcruntime_typeinfo.h> // type_info::operator==
#include <vcruntime_typeinfo.h> // type_info::raw_name

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_vcruntime_typeinfo(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // type_info file:vcruntime_typeinfo.h line:62
		pybind11::class_<type_info, std::shared_ptr<type_info>> cl(M(""), "type_info", "");
		pybind11::handle cl_type = cl;

		cl.def("hash_code", (unsigned int (type_info::*)() const) &type_info::hash_code, "C++: type_info::hash_code() const --> unsigned int");
		cl.def("__eq__", (bool (type_info::*)(const class type_info &) const) &type_info::operator==, "C++: type_info::operator==(const class type_info &) const --> bool", pybind11::arg("_Other"));
		cl.def("__ne__", (bool (type_info::*)(const class type_info &) const) &type_info::operator!=, "C++: type_info::operator!=(const class type_info &) const --> bool", pybind11::arg("_Other"));
		cl.def("before", (bool (type_info::*)(const class type_info &) const) &type_info::before, "C++: type_info::before(const class type_info &) const --> bool", pybind11::arg("_Other"));
		cl.def("name", (const char * (type_info::*)() const) &type_info::name, "C++: type_info::name() const --> const char *", pybind11::return_value_policy::automatic);
		cl.def("raw_name", (const char * (type_info::*)() const) &type_info::raw_name, "C++: type_info::raw_name() const --> const char *", pybind11::return_value_policy::automatic);
	}
}


// File: unknown/unknown_1.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // UUL_Entry
#include <../plugins/plugin.h> // UUL_Entry::UUL_Entry
#include <initializer_list> // std::initializer_list
#include <memory> // std::allocator
#include <set> // std::set
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::operator=
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::set
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::swap
#include <sstream> // __str__
#include <utility> // std::pair
#include <xstddef> // std::less

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_unknown_unknown_1(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // tcl::associative_tree file: line:55
		pybind11::class_<tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>, tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>*, tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>> cl(M("tcl"), "associative_tree_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_std_set_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_tcl_tree_deref_less_UUL_Entry_std_less_UUL_Entry_std_allocator_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_t", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<const class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > &>(), pybind11::arg(""));

		cl.def("begin", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::begin, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::begin() const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>");
		cl.def("end", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::end, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::end() const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>");
		cl.def("begin", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::begin, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::begin() --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>");
		cl.def("end", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::end, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::end() --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>");
		cl.def("find", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::find, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::find(const struct UUL_Entry &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("find", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::find, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::find(const struct UUL_Entry &) const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("erase", (bool (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase(const struct UUL_Entry &) --> bool", pybind11::arg("value"));
		cl.def("erase", (void (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>) --> void", pybind11::arg("it"));
		cl.def("erase", (void (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>) --> void", pybind11::arg("it_beg"), pybind11::arg("it_end"));
		cl.def("clear", (void (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::clear, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::clear() --> void");
		cl.def("count", (unsigned int (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::count, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::count(const struct UUL_Entry &) const --> unsigned int", pybind11::arg("value"));
		cl.def("lower_bound", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::lower_bound, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::lower_bound(const struct UUL_Entry &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("lower_bound", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::lower_bound, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::lower_bound(const struct UUL_Entry &) const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("upper_bound", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::upper_bound, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::upper_bound(const struct UUL_Entry &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("upper_bound", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::upper_bound, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::upper_bound(const struct UUL_Entry &) const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("equal_range", (struct std::pair<class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> > (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::equal_range, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::equal_range(const struct UUL_Entry &) --> struct std::pair<class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> >", pybind11::arg("value"));
		cl.def("equal_range", (struct std::pair<class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> > (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::equal_range, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::equal_range(const struct UUL_Entry &) const --> struct std::pair<class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> >", pybind11::arg("value"));
		cl.def("assign", (class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > & (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::operator=, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::operator=(const class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > &) --> class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("get", (const struct UUL_Entry * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get() const --> const struct UUL_Entry *", pybind11::return_value_policy::automatic);
		cl.def("get", (struct UUL_Entry * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get() --> struct UUL_Entry *", pybind11::return_value_policy::automatic);
		cl.def("is_root", (bool (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::is_root, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::is_root() const --> bool");
		cl.def("size", (unsigned int (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::size, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::size() const --> unsigned int");
		cl.def("max_size", (unsigned int (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::max_size, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::max_size() const --> unsigned int");
		cl.def("empty", (bool (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::empty, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::empty() const --> bool");
		cl.def("parent", (class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent() --> class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic);
		cl.def("parent", (const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent() const --> const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic);
	}
}


// File: unknown/unknown_2.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // UUL_Entry
#include <../plugins/plugin.h> // UUL_Entry::UUL_Entry
#include <initializer_list> // std::initializer_list
#include <memory> // std::allocator
#include <set> // std::set
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::operator=
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::set
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::swap
#include <sstream> // __str__
#include <xstddef> // std::less

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_unknown_unknown_2(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // tcl::associative_iterator file: line:70
		pybind11::class_<tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>, std::shared_ptr<tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>>, std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,const UUL_Entry *,const UUL_Entry &>> cl(M("tcl"), "associative_iterator_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_const_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_std_set_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_tcl_tree_deref_less_UUL_Entry_std_less_UUL_Entry_std_allocator_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_const_UUL_Entry_*_const_UUL_Entry_&_t", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());

		cl.def(pybind11::init<const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &>(), pybind11::arg("src"));

		cl.def(pybind11::init<const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &>(), pybind11::arg(""));

		cl.def("__mul__", (const struct UUL_Entry & (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)() const) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator*, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator*() const --> const struct UUL_Entry &", pybind11::return_value_policy::automatic);
		cl.def("plus_plus", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> & (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)()) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator++, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator++() --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &", pybind11::return_value_policy::automatic);
		cl.def("plus_plus", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)(int)) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator++, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator++(int) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>", pybind11::arg(""));
		cl.def("minus_minus", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> & (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)()) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator--, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator--() --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &", pybind11::return_value_policy::automatic);
		cl.def("minus_minus", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)(int)) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator--, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator--(int) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>", pybind11::arg(""));
		cl.def("node", (const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)() const) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::node, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::node() const --> const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic);
		cl.def("__eq__", (bool (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) const) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator==, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator==(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) const --> bool", pybind11::arg("rhs"));
		cl.def("__ne__", (bool (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) const) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator!=, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator!=(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) const --> bool", pybind11::arg("rhs"));
		cl.def("assign", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> & (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &>::*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &)) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator=, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, const UUL_Entry *, const UUL_Entry &>::operator=(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> & (std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,const UUL_Entry *,const UUL_Entry &>::*)(const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> &)) &std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, const UUL_Entry *, const UUL_Entry &>::operator=, "C++: std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, const UUL_Entry *, const UUL_Entry &>::operator=(const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> &) --> struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, const struct UUL_Entry *, const struct UUL_Entry &> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}


// File: unknown/unknown_3.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../include/tcl/tree.h> // tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >::operator()
#include <../include/tcl/tree.h> // tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >::tree_deref_less
#include <../plugins/plugin.h> // UUL_Entry
#include <../plugins/plugin.h> // UUL_Entry::UUL_Entry
#include <initializer_list> // std::initializer_list
#include <initializer_list> // std::initializer_list<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::initializer_list
#include <initializer_list> // std::initializer_list<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::size
#include <memory> // std::allocator
#include <memory> // std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::address
#include <memory> // std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::allocate
#include <memory> // std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::allocator
#include <memory> // std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *>::max_size
#include <set> // std::set
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::operator=
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::set
#include <set> // std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >::swap
#include <sstream> // __str__
#include <xstddef> // std::less
#include <xstddef> // std::less<UUL_Entry>::operator()

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_unknown_unknown_3(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	// tcl::associative_it_init(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &) file: line:127
	M("tcl").def("associative_it_init_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_const_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_std_set_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_tcl_tree_deref_less_UUL_Entry_std_less_UUL_Entry_std_allocator_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_const_UUL_Entry_*_const_UUL_Entry_&_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_UUL_Entry_*_UUL_Entry_&_t", (void (*)(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &)) &tcl::associative_it_init<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,UUL_Entry *,UUL_Entry &>, "C++: tcl::associative_it_init(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &) --> void", pybind11::arg("dest"), pybind11::arg("src"));

	// tcl::associative_it_init(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &) file: line:128
	M("tcl").def("associative_it_init_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_std_set_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_tcl_tree_deref_less_UUL_Entry_std_less_UUL_Entry_std_allocator_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_UUL_Entry_*_UUL_Entry_&_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_UUL_Entry_*_UUL_Entry_&_t", (void (*)(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &)) &tcl::associative_it_init<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,UUL_Entry *,UUL_Entry &>, "C++: tcl::associative_it_init(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &) --> void", pybind11::arg("dest"), pybind11::arg("src"));

	// tcl::associative_it_eq(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) file: line:129
	M("tcl").def("associative_it_eq_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_const_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_std_set_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_tcl_tree_deref_less_UUL_Entry_std_less_UUL_Entry_std_allocator_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_const_UUL_Entry_*_const_UUL_Entry_&_const_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_const_UUL_Entry_*_const_UUL_Entry_&_t", (bool (*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &)) &tcl::associative_it_eq<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,const UUL_Entry *,const UUL_Entry &,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,const UUL_Entry *,const UUL_Entry &>, "C++: tcl::associative_it_eq(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) --> bool", pybind11::arg(""), pybind11::arg(""));

	// tcl::associative_it_eq(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) file: line:130
	M("tcl").def("associative_it_eq_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_std_set_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_tcl_tree_deref_less_UUL_Entry_std_less_UUL_Entry_std_allocator_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_UUL_Entry_*_UUL_Entry_&_const_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_const_UUL_Entry_*_const_UUL_Entry_&_t", (bool (*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &)) &tcl::associative_it_eq<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &,const tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,const UUL_Entry *,const UUL_Entry &>, "C++: tcl::associative_it_eq(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> *, const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) --> bool", pybind11::arg("lhs"), pybind11::arg("rhs"));

	{ // tcl::associative_iterator file: line:70
		pybind11::class_<tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>, std::shared_ptr<tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>>, std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,UUL_Entry *,UUL_Entry &>> cl(M("tcl"), "associative_iterator_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_std_set_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_tcl_tree_deref_less_UUL_Entry_std_less_UUL_Entry_std_allocator_tcl_tree_UUL_Entry_std_less_UUL_Entry_*_UUL_Entry_*_UUL_Entry_&_t", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());

		cl.def(pybind11::init<const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &>(), pybind11::arg("src"));

		cl.def("__mul__", (struct UUL_Entry & (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)() const) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator*, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator*() const --> struct UUL_Entry &", pybind11::return_value_policy::automatic);
		cl.def("plus_plus", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> & (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)()) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator++, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator++() --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &", pybind11::return_value_policy::automatic);
		cl.def("plus_plus", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)(int)) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator++, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator++(int) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg(""));
		cl.def("minus_minus", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> & (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)()) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator--, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator--() --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &", pybind11::return_value_policy::automatic);
		cl.def("minus_minus", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)(int)) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator--, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator--(int) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg(""));
		cl.def("node", (class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)() const) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::node, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::node() const --> class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic);
		cl.def("__eq__", (bool (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) const) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator==, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator==(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) const --> bool", pybind11::arg("rhs"));
		cl.def("__ne__", (bool (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) const) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator!=, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator!=(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> &) const --> bool", pybind11::arg("rhs"));
		cl.def("assign", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> & (tcl::associative_iterator<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,tcl::tree<UUL_Entry, std::less<UUL_Entry> > *,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >,UUL_Entry *,UUL_Entry &>::*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &)) &tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator=, "C++: tcl::associative_iterator<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >, UUL_Entry *, UUL_Entry &>::operator=(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> & (std::iterator<std::bidirectional_iterator_tag,UUL_Entry,int,UUL_Entry *,UUL_Entry &>::*)(const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> &)) &std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, UUL_Entry *, UUL_Entry &>::operator=, "C++: std::iterator<std::bidirectional_iterator_tag, UUL_Entry, int, UUL_Entry *, UUL_Entry &>::operator=(const struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> &) --> struct std::iterator<struct std::bidirectional_iterator_tag, struct UUL_Entry, int, struct UUL_Entry *, struct UUL_Entry &> &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
}


// File: __/include/tcl/tree.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // UUL_Entry
#include <../plugins/plugin.h> // UUL_Entry::UUL_Entry
#include <memory> // std::allocator
#include <set> // std::set
#include <sstream> // __str__
#include <xstddef> // std::less
#include <xstddef> // std::less<UUL_Entry>::operator()

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind____include_tcl_tree(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // tcl::tree file:../include/tcl/tree.h line:54
		pybind11::class_<tcl::tree<UUL_Entry,std::less<UUL_Entry>>, std::shared_ptr<tcl::tree<UUL_Entry,std::less<UUL_Entry>>>, tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>> cl(M("tcl"), "tree_UUL_Entry_std_less_UUL_Entry_t", "");
		pybind11::handle cl_type = cl;

		cl.def("__init__", [](tcl::tree<UUL_Entry,std::less<UUL_Entry>> *self_) { new (self_) tcl::tree<UUL_Entry,std::less<UUL_Entry>>(); }, "doc");
		cl.def(pybind11::init<const struct UUL_Entry &>(), pybind11::arg("value"));

		cl.def(pybind11::init<const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &>(), pybind11::arg("rhs"));

		cl.def("assign", (class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > & (tcl::tree<UUL_Entry,std::less<UUL_Entry>>::*)(const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &)) &tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=, "C++: tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=(const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &) --> class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &", pybind11::return_value_policy::automatic, pybind11::arg("rhs"));
		cl.def("insert", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::tree<UUL_Entry,std::less<UUL_Entry>>::*)(const struct UUL_Entry &)) &tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert, "C++: tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert(const struct UUL_Entry &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("insert", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::tree<UUL_Entry,std::less<UUL_Entry>>::*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>, const struct UUL_Entry &)) &tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert, "C++: tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>, const struct UUL_Entry &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("pos"), pybind11::arg("value"));
		cl.def("insert", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::tree<UUL_Entry,std::less<UUL_Entry>>::*)(const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &)) &tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert, "C++: tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert(const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("tree_obj"));
		cl.def("insert", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::tree<UUL_Entry,std::less<UUL_Entry>>::*)(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &)) &tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert, "C++: tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert(const class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("pos"), pybind11::arg("tree_obj"));
		cl.def("swap", (void (tcl::tree<UUL_Entry,std::less<UUL_Entry>>::*)(class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &)) &tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap, "C++: tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap(class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > &) --> void", pybind11::arg("rhs"));
		cl.def("begin", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::begin, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::begin() const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>");
		cl.def("end", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::end, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::end() const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>");
		cl.def("begin", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::begin, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::begin() --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>");
		cl.def("end", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::end, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::end() --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>");
		cl.def("find", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::find, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::find(const struct UUL_Entry &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("find", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::find, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::find(const struct UUL_Entry &) const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("erase", (bool (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase(const struct UUL_Entry &) --> bool", pybind11::arg("value"));
		cl.def("erase", (void (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>) --> void", pybind11::arg("it"));
		cl.def("erase", (void (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::erase(class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>) --> void", pybind11::arg("it_beg"), pybind11::arg("it_end"));
		cl.def("clear", (void (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::clear, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::clear() --> void");
		cl.def("count", (unsigned int (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::count, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::count(const struct UUL_Entry &) const --> unsigned int", pybind11::arg("value"));
		cl.def("lower_bound", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::lower_bound, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::lower_bound(const struct UUL_Entry &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("lower_bound", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::lower_bound, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::lower_bound(const struct UUL_Entry &) const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("upper_bound", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::upper_bound, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::upper_bound(const struct UUL_Entry &) --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("upper_bound", (class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::upper_bound, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::upper_bound(const struct UUL_Entry &) const --> class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>", pybind11::arg("value"));
		cl.def("equal_range", (struct std::pair<class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> > (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::equal_range, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::equal_range(const struct UUL_Entry &) --> struct std::pair<class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, struct UUL_Entry *, struct UUL_Entry &> >", pybind11::arg("value"));
		cl.def("equal_range", (struct std::pair<class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> > (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const struct UUL_Entry &) const) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::equal_range, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::equal_range(const struct UUL_Entry &) const --> struct std::pair<class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &>, class tcl::associative_iterator<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> >, const struct UUL_Entry *, const struct UUL_Entry &> >", pybind11::arg("value"));
		cl.def("assign", (class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > & (tcl::associative_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)(const class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > &)) &tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::operator=, "C++: tcl::associative_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::operator=(const class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > &) --> class tcl::associative_tree<struct UUL_Entry, class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::set<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *, struct tcl::tree_deref_less<struct UUL_Entry, struct std::less<struct UUL_Entry> >, class std::allocator<class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *> > > &", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("get", (const struct UUL_Entry * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get() const --> const struct UUL_Entry *", pybind11::return_value_policy::automatic);
		cl.def("get", (struct UUL_Entry * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::get() --> struct UUL_Entry *", pybind11::return_value_policy::automatic);
		cl.def("is_root", (bool (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::is_root, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::is_root() const --> bool");
		cl.def("size", (unsigned int (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::size, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::size() const --> unsigned int");
		cl.def("max_size", (unsigned int (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::max_size, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::max_size() const --> unsigned int");
		cl.def("empty", (bool (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::empty, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::empty() const --> bool");
		cl.def("parent", (class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)()) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent() --> class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic);
		cl.def("parent", (const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (tcl::basic_tree<UUL_Entry,tcl::tree<UUL_Entry, std::less<UUL_Entry> >,std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> >>::*)() const) &tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent, "C++: tcl::basic_tree<UUL_Entry, tcl::tree<UUL_Entry, std::less<UUL_Entry> >, std::set<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *, tcl::tree_deref_less<UUL_Entry, std::less<UUL_Entry> >, std::allocator<tcl::tree<UUL_Entry, std::less<UUL_Entry> > *> > >::parent() const --> const class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic);
	}
}


// File: std/memory.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // StorageState
#include <../plugins/plugin.h> // StorageState::StorageState
#include <../plugins/plugin.h> // UUL_Entry
#include <memory> // std::_Ptr_base
#include <memory> // std::_Ptr_base<DirectMemory>::_Decref
#include <memory> // std::_Ptr_base<DirectMemory>::_Decwref
#include <memory> // std::_Ptr_base<DirectMemory>::_Expired
#include <memory> // std::_Ptr_base<DirectMemory>::_Get
#include <memory> // std::_Ptr_base<DirectMemory>::_Get_deleter
#include <memory> // std::_Ptr_base<DirectMemory>::_Ptr_base
#include <memory> // std::_Ptr_base<DirectMemory>::_Reset
#include <memory> // std::_Ptr_base<DirectMemory>::_Reset0
#include <memory> // std::_Ptr_base<DirectMemory>::_Resetw
#include <memory> // std::_Ptr_base<DirectMemory>::_Swap
#include <memory> // std::_Ptr_base<DirectMemory>::use_count
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Decref
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Decwref
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Expired
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Get
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Get_deleter
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Ptr_base
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Reset
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Reset0
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Resetw
#include <memory> // std::_Ptr_base<MemoryMappedFile>::_Swap
#include <memory> // std::_Ptr_base<MemoryMappedFile>::use_count
#include <memory> // std::_Ptr_base<MemoryRegion>::_Decref
#include <memory> // std::_Ptr_base<MemoryRegion>::_Decwref
#include <memory> // std::_Ptr_base<MemoryRegion>::_Expired
#include <memory> // std::_Ptr_base<MemoryRegion>::_Get
#include <memory> // std::_Ptr_base<MemoryRegion>::_Get_deleter
#include <memory> // std::_Ptr_base<MemoryRegion>::_Ptr_base
#include <memory> // std::_Ptr_base<MemoryRegion>::_Reset
#include <memory> // std::_Ptr_base<MemoryRegion>::_Reset0
#include <memory> // std::_Ptr_base<MemoryRegion>::_Resetw
#include <memory> // std::_Ptr_base<MemoryRegion>::_Swap
#include <memory> // std::_Ptr_base<MemoryRegion>::use_count
#include <memory> // std::_Ptr_base<StorageState>::_Decref
#include <memory> // std::_Ptr_base<StorageState>::_Decwref
#include <memory> // std::_Ptr_base<StorageState>::_Expired
#include <memory> // std::_Ptr_base<StorageState>::_Get
#include <memory> // std::_Ptr_base<StorageState>::_Get_deleter
#include <memory> // std::_Ptr_base<StorageState>::_Ptr_base
#include <memory> // std::_Ptr_base<StorageState>::_Reset
#include <memory> // std::_Ptr_base<StorageState>::_Reset0
#include <memory> // std::_Ptr_base<StorageState>::_Resetw
#include <memory> // std::_Ptr_base<StorageState>::_Swap
#include <memory> // std::_Ptr_base<StorageState>::use_count
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Decref
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Decwref
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Expired
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Get
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Get_deleter
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Ptr_base
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Reset
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Reset0
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Resetw
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::_Swap
#include <memory> // std::_Ptr_base<binpac::IMemoryStorage>::use_count
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Decref
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Decwref
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Expired
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Get
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Get_deleter
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Ptr_base
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Reset
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Reset0
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Resetw
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::_Swap
#include <memory> // std::_Ptr_base<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::use_count
#include <memory> // std::_Ref_count_base
#include <memory> // std::_Ref_count_base::_Decref
#include <memory> // std::_Ref_count_base::_Decwref
#include <memory> // std::_Ref_count_base::_Expired
#include <memory> // std::_Ref_count_base::_Get_deleter
#include <memory> // std::_Ref_count_base::_Incref
#include <memory> // std::_Ref_count_base::_Incref_nz
#include <memory> // std::_Ref_count_base::_Incwref
#include <memory> // std::_Ref_count_base::_Use_count
#include <memory> // std::_Ref_count_base::operator=
#include <memory> // std::_Shared_ptr_spin_lock
#include <memory> // std::_Shared_ptr_spin_lock::_Shared_ptr_spin_lock
#include <memory> // std::align
#include <memory> // std::allocator
#include <memory> // std::declare_no_pointers
#include <memory> // std::declare_reachable
#include <memory> // std::get_pointer_safety
#include <memory> // std::owner_less
#include <memory> // std::pointer_safety
#include <memory> // std::undeclare_no_pointers
#include <set> // std::set
#include <sstream> // __str__
#include <string> // std::basic_string
#include <string> // std::char_traits
#include <vcruntime_typeinfo.h> // type_info
#include <vcruntime_typeinfo.h> // type_info::before
#include <vcruntime_typeinfo.h> // type_info::hash_code
#include <vcruntime_typeinfo.h> // type_info::name
#include <vcruntime_typeinfo.h> // type_info::operator!=
#include <vcruntime_typeinfo.h> // type_info::operator==
#include <vcruntime_typeinfo.h> // type_info::raw_name
#include <xstddef> // std::less

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_std_memory(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // std::_Ref_count_base file:memory line:47
		pybind11::class_<std::_Ref_count_base, std::shared_ptr<std::_Ref_count_base>> cl(M("std"), "_Ref_count_base", "");
		pybind11::handle cl_type = cl;

		cl.def("_Incref_nz", (bool (std::_Ref_count_base::*)()) &std::_Ref_count_base::_Incref_nz, "C++: std::_Ref_count_base::_Incref_nz() --> bool");
		cl.def("_Incref", (void (std::_Ref_count_base::*)()) &std::_Ref_count_base::_Incref, "C++: std::_Ref_count_base::_Incref() --> void");
		cl.def("_Incwref", (void (std::_Ref_count_base::*)()) &std::_Ref_count_base::_Incwref, "C++: std::_Ref_count_base::_Incwref() --> void");
		cl.def("_Decref", (void (std::_Ref_count_base::*)()) &std::_Ref_count_base::_Decref, "C++: std::_Ref_count_base::_Decref() --> void");
		cl.def("_Decwref", (void (std::_Ref_count_base::*)()) &std::_Ref_count_base::_Decwref, "C++: std::_Ref_count_base::_Decwref() --> void");
		cl.def("_Use_count", (long (std::_Ref_count_base::*)() const) &std::_Ref_count_base::_Use_count, "C++: std::_Ref_count_base::_Use_count() const --> long");
		cl.def("_Expired", (bool (std::_Ref_count_base::*)() const) &std::_Ref_count_base::_Expired, "C++: std::_Ref_count_base::_Expired() const --> bool");
		cl.def("_Get_deleter", (void * (std::_Ref_count_base::*)(const class type_info &) const) &std::_Ref_count_base::_Get_deleter, "C++: std::_Ref_count_base::_Get_deleter(const class type_info &) const --> void *", pybind11::return_value_policy::automatic, pybind11::arg(""));
		cl.def("assign", (class std::_Ref_count_base & (std::_Ref_count_base::*)(const class std::_Ref_count_base &)) &std::_Ref_count_base::operator=, "C++: std::_Ref_count_base::operator=(const class std::_Ref_count_base &) --> class std::_Ref_count_base &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	{ // std::_Ptr_base file:memory line:266
		pybind11::class_<std::_Ptr_base<binpac::IMemoryStorage>, std::shared_ptr<std::_Ptr_base<binpac::IMemoryStorage>>> cl(M("std"), "_Ptr_base_binpac_IMemoryStorage_t", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());

		cl.def("use_count", (long (std::_Ptr_base<binpac::IMemoryStorage>::*)() const) &std::_Ptr_base<binpac::IMemoryStorage>::use_count, "C++: std::_Ptr_base<binpac::IMemoryStorage>::use_count() const --> long");
		cl.def("_Swap", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)(class std::_Ptr_base<class binpac::IMemoryStorage> &)) &std::_Ptr_base<binpac::IMemoryStorage>::_Swap, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Swap(class std::_Ptr_base<class binpac::IMemoryStorage> &) --> void", pybind11::arg("_Right"));
		cl.def("_Get_deleter", (void * (std::_Ptr_base<binpac::IMemoryStorage>::*)(const class type_info &) const) &std::_Ptr_base<binpac::IMemoryStorage>::_Get_deleter, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Get_deleter(const class type_info &) const --> void *", pybind11::return_value_policy::automatic, pybind11::arg("_Typeid"));
		cl.def("_Get", (class binpac::IMemoryStorage * (std::_Ptr_base<binpac::IMemoryStorage>::*)() const) &std::_Ptr_base<binpac::IMemoryStorage>::_Get, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Get() const --> class binpac::IMemoryStorage *", pybind11::return_value_policy::automatic);
		cl.def("_Expired", (bool (std::_Ptr_base<binpac::IMemoryStorage>::*)() const) &std::_Ptr_base<binpac::IMemoryStorage>::_Expired, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Expired() const --> bool");
		cl.def("_Decref", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)()) &std::_Ptr_base<binpac::IMemoryStorage>::_Decref, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Decref() --> void");
		cl.def("_Reset", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)()) &std::_Ptr_base<binpac::IMemoryStorage>::_Reset, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Reset() --> void");
		cl.def("_Reset", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)(class binpac::IMemoryStorage *, class std::_Ref_count_base *)) &std::_Ptr_base<binpac::IMemoryStorage>::_Reset, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Reset(class binpac::IMemoryStorage *, class std::_Ref_count_base *) --> void", pybind11::arg("_Other_ptr"), pybind11::arg("_Other_rep"));
		cl.def("_Reset", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)(class binpac::IMemoryStorage *, class std::_Ref_count_base *, bool)) &std::_Ptr_base<binpac::IMemoryStorage>::_Reset, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Reset(class binpac::IMemoryStorage *, class std::_Ref_count_base *, bool) --> void", pybind11::arg("_Other_ptr"), pybind11::arg("_Other_rep"), pybind11::arg("_Throw"));
		cl.def("_Reset0", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)(class binpac::IMemoryStorage *, class std::_Ref_count_base *)) &std::_Ptr_base<binpac::IMemoryStorage>::_Reset0, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Reset0(class binpac::IMemoryStorage *, class std::_Ref_count_base *) --> void", pybind11::arg("_Other_ptr"), pybind11::arg("_Other_rep"));
		cl.def("_Decwref", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)()) &std::_Ptr_base<binpac::IMemoryStorage>::_Decwref, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Decwref() --> void");
		cl.def("_Resetw", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)()) &std::_Ptr_base<binpac::IMemoryStorage>::_Resetw, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Resetw() --> void");
	}
}


// File: unknown/unknown_4.cpp
#include <memory> // std::shared_ptr
#include <memory> // std::weak_ptr
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::expired
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::lock
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::operator=
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::reset
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::swap
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::weak_ptr
#include <sstream> // __str__

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

// binpac::IMemoryStorage file: line:45
struct PyCallBack_IMemoryStorage : public binpac::IMemoryStorage {
	using binpac::IMemoryStorage::IMemoryStorage;

	void SetPage(unsigned int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "SetPage");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::SetPage\"");
	}
	unsigned int GetPageSize() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "GetPageSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::GetPageSize\"");
	}
	unsigned int GetSize() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "GetSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::GetSize\"");
	}
	void SetSize(unsigned int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "SetSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::SetSize\"");
	}
	class binpac::offset_ptr Begin() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "Begin");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<class binpac::offset_ptr>::value) {
				static pybind11::detail::overload_caster_t<class binpac::offset_ptr> caster;
				return pybind11::detail::cast_ref<class binpac::offset_ptr>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<class binpac::offset_ptr>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::Begin\"");
	}
	class binpac::offset_ptr End() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "End");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<class binpac::offset_ptr>::value) {
				static pybind11::detail::overload_caster_t<class binpac::offset_ptr> caster;
				return pybind11::detail::cast_ref<class binpac::offset_ptr>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<class binpac::offset_ptr>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::End\"");
	}
	unsigned char * BeginPtr() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "BeginPtr");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned char *>::value) {
				static pybind11::detail::overload_caster_t<unsigned char *> caster;
				return pybind11::detail::cast_ref<unsigned char *>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned char *>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::BeginPtr\"");
	}
	unsigned int GetCurrentPageId() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "GetCurrentPageId");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::GetCurrentPageId\"");
	}
	unsigned char * GetTempBufferPtr() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "GetTempBufferPtr");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned char *>::value) {
				static pybind11::detail::overload_caster_t<unsigned char *> caster;
				return pybind11::detail::cast_ref<unsigned char *>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned char *>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::GetTempBufferPtr\"");
	}
	unsigned int GetCurrentPageSize() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "GetCurrentPageSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::GetCurrentPageSize\"");
	}
	void AddStorePiece(unsigned int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "AddStorePiece");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::AddStorePiece\"");
	}
	unsigned int ReadUInt8(unsigned char * a0, unsigned int a1) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "ReadUInt8");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::ReadUInt8\"");
	}
	unsigned int Read(unsigned char * a0, class binpac::offset_ptr a1, unsigned int a2) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "Read");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1, a2);
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::Read\"");
	}
	unsigned int ReadUInt32(unsigned int * a0, unsigned int a1) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "ReadUInt32");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::ReadUInt32\"");
	}
	unsigned int WriteUInt32(unsigned int a0, unsigned int a1) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const binpac::IMemoryStorage *>(this), "WriteUInt32");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0, a1);
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		pybind11::pybind11_fail("Tried to call pure virtual function \"IMemoryStorage::WriteUInt32\"");
	}
};

void bind_unknown_unknown_4(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // binpac::IMemoryStorage file: line:45
		pybind11::class_<binpac::IMemoryStorage, std::shared_ptr<binpac::IMemoryStorage>, PyCallBack_IMemoryStorage> cl(M("binpac"), "IMemoryStorage", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());
		cl.def("SetPage", (void (binpac::IMemoryStorage::*)(unsigned int)) &binpac::IMemoryStorage::SetPage, "C++: binpac::IMemoryStorage::SetPage(unsigned int) --> void", pybind11::arg(""));
		cl.def("GetPageSize", (unsigned int (binpac::IMemoryStorage::*)()) &binpac::IMemoryStorage::GetPageSize, "C++: binpac::IMemoryStorage::GetPageSize() --> unsigned int");
		cl.def("GetSize", (unsigned int (binpac::IMemoryStorage::*)()) &binpac::IMemoryStorage::GetSize, "C++: binpac::IMemoryStorage::GetSize() --> unsigned int");
		cl.def("SetSize", (void (binpac::IMemoryStorage::*)(unsigned int)) &binpac::IMemoryStorage::SetSize, "C++: binpac::IMemoryStorage::SetSize(unsigned int) --> void", pybind11::arg(""));
		cl.def("Begin", (class binpac::offset_ptr (binpac::IMemoryStorage::*)()) &binpac::IMemoryStorage::Begin, "C++: binpac::IMemoryStorage::Begin() --> class binpac::offset_ptr");
		cl.def("End", (class binpac::offset_ptr (binpac::IMemoryStorage::*)()) &binpac::IMemoryStorage::End, "C++: binpac::IMemoryStorage::End() --> class binpac::offset_ptr");
		cl.def("BeginPtr", (unsigned char * (binpac::IMemoryStorage::*)()) &binpac::IMemoryStorage::BeginPtr, "C++: binpac::IMemoryStorage::BeginPtr() --> unsigned char *", pybind11::return_value_policy::automatic);
		cl.def("GetCurrentPageId", (unsigned int (binpac::IMemoryStorage::*)()) &binpac::IMemoryStorage::GetCurrentPageId, "C++: binpac::IMemoryStorage::GetCurrentPageId() --> unsigned int");
		cl.def("GetTempBufferPtr", (unsigned char * (binpac::IMemoryStorage::*)()) &binpac::IMemoryStorage::GetTempBufferPtr, "C++: binpac::IMemoryStorage::GetTempBufferPtr() --> unsigned char *", pybind11::return_value_policy::automatic);
		cl.def("GetCurrentPageSize", (unsigned int (binpac::IMemoryStorage::*)()) &binpac::IMemoryStorage::GetCurrentPageSize, "C++: binpac::IMemoryStorage::GetCurrentPageSize() --> unsigned int");
		cl.def("AddStorePiece", (void (binpac::IMemoryStorage::*)(unsigned int)) &binpac::IMemoryStorage::AddStorePiece, "C++: binpac::IMemoryStorage::AddStorePiece(unsigned int) --> void", pybind11::arg(""));
		cl.def("ReadUInt8", (unsigned int (binpac::IMemoryStorage::*)(unsigned char *, unsigned int)) &binpac::IMemoryStorage::ReadUInt8, "C++: binpac::IMemoryStorage::ReadUInt8(unsigned char *, unsigned int) --> unsigned int", pybind11::arg(""), pybind11::arg(""));
		cl.def("Read", (unsigned int (binpac::IMemoryStorage::*)(unsigned char *, class binpac::offset_ptr, unsigned int)) &binpac::IMemoryStorage::Read, "C++: binpac::IMemoryStorage::Read(unsigned char *, class binpac::offset_ptr, unsigned int) --> unsigned int", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""));
		cl.def("ReadUInt32", (unsigned int (binpac::IMemoryStorage::*)(unsigned int *, unsigned int)) &binpac::IMemoryStorage::ReadUInt32, "C++: binpac::IMemoryStorage::ReadUInt32(unsigned int *, unsigned int) --> unsigned int", pybind11::arg(""), pybind11::arg(""));
		cl.def("WriteUInt32", (unsigned int (binpac::IMemoryStorage::*)(unsigned int, unsigned int)) &binpac::IMemoryStorage::WriteUInt32, "C++: binpac::IMemoryStorage::WriteUInt32(unsigned int, unsigned int) --> unsigned int", pybind11::arg(""), pybind11::arg(""));
		cl.def("assign", (class binpac::IMemoryStorage & (binpac::IMemoryStorage::*)(const class binpac::IMemoryStorage &)) &binpac::IMemoryStorage::operator=, "C++: binpac::IMemoryStorage::operator=(const class binpac::IMemoryStorage &) --> class binpac::IMemoryStorage &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	{ // binpac::offset_ptr file: line:7
		pybind11::class_<binpac::offset_ptr, std::shared_ptr<binpac::offset_ptr>> cl(M("binpac"), "offset_ptr", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());

		cl.def(pybind11::init<const unsigned char *>(), pybind11::arg("ptr"));

		cl.def(pybind11::init<const unsigned char *, class std::weak_ptr<class binpac::IMemoryStorage>>(), pybind11::arg("ptr"), pybind11::arg("w_mem_strg"));

		cl.def(pybind11::init<const class binpac::offset_ptr &>(), pybind11::arg("ptr"));

		cl.def_readwrite("page_", &binpac::offset_ptr::page_);
		cl.def_readwrite("offset_", &binpac::offset_ptr::offset_);
		cl.def_readwrite("mem_strg_", &binpac::offset_ptr::mem_strg_);
		cl.def("get_fictive_ptr", (unsigned char * (binpac::offset_ptr::*)() const) &binpac::offset_ptr::get_fictive_ptr, "C++: binpac::offset_ptr::get_fictive_ptr() const --> unsigned char *", pybind11::return_value_policy::automatic);
		cl.def("get_ptr", (unsigned char * (binpac::offset_ptr::*)() const) &binpac::offset_ptr::get_ptr, "C++: binpac::offset_ptr::get_ptr() const --> unsigned char *", pybind11::return_value_policy::automatic);
		cl.def("plus_plus", (const class binpac::offset_ptr & (binpac::offset_ptr::*)() const) &binpac::offset_ptr::operator++, "C++: binpac::offset_ptr::operator++() const --> const class binpac::offset_ptr &", pybind11::return_value_policy::automatic);
		cl.def("__mul__", (unsigned char & (binpac::offset_ptr::*)()) &binpac::offset_ptr::operator*, "C++: binpac::offset_ptr::operator*() --> unsigned char &", pybind11::return_value_policy::automatic);
		cl.def("__eq__", (bool (binpac::offset_ptr::*)(const class binpac::offset_ptr &) const) &binpac::offset_ptr::operator==, "C++: binpac::offset_ptr::operator==(const class binpac::offset_ptr &) const --> bool", pybind11::arg("c2"));
		cl.def("__add__", (int (binpac::offset_ptr::*)(const class binpac::offset_ptr &) const) &binpac::offset_ptr::operator+, "C++: binpac::offset_ptr::operator+(const class binpac::offset_ptr &) const --> int", pybind11::arg("c2"));
		cl.def("__sub__", (int (binpac::offset_ptr::*)(const class binpac::offset_ptr &) const) &binpac::offset_ptr::operator-, "C++: binpac::offset_ptr::operator-(const class binpac::offset_ptr &) const --> int", pybind11::arg("c2"));
		cl.def("__add__", (class binpac::offset_ptr (binpac::offset_ptr::*)(const unsigned int &) const) &binpac::offset_ptr::operator+, "C++: binpac::offset_ptr::operator+(const unsigned int &) const --> class binpac::offset_ptr", pybind11::arg("c2"));
		cl.def("__sub__", (class binpac::offset_ptr (binpac::offset_ptr::*)(const unsigned int &) const) &binpac::offset_ptr::operator-, "C++: binpac::offset_ptr::operator-(const unsigned int &) const --> class binpac::offset_ptr", pybind11::arg("c2"));
		cl.def("__iadd__", (const class binpac::offset_ptr & (binpac::offset_ptr::*)(const unsigned int &) const) &binpac::offset_ptr::operator+=, "C++: binpac::offset_ptr::operator+=(const unsigned int &) const --> const class binpac::offset_ptr &", pybind11::return_value_policy::automatic, pybind11::arg("c2"));
		cl.def("assign", (const class binpac::offset_ptr & (binpac::offset_ptr::*)(const class binpac::offset_ptr &) const) &binpac::offset_ptr::operator=, "C++: binpac::offset_ptr::operator=(const class binpac::offset_ptr &) const --> const class binpac::offset_ptr &", pybind11::return_value_policy::automatic, pybind11::arg("c2"));
		cl.def("set_mem_strg", (void (binpac::offset_ptr::*)(class std::weak_ptr<class binpac::IMemoryStorage>) const) &binpac::offset_ptr::set_mem_strg, "C++: binpac::offset_ptr::set_mem_strg(class std::weak_ptr<class binpac::IMemoryStorage>) const --> void", pybind11::arg("mem_strg"));
		cl.def("mem_strg", (class std::weak_ptr<class binpac::IMemoryStorage> (binpac::offset_ptr::*)() const) &binpac::offset_ptr::mem_strg, "C++: binpac::offset_ptr::mem_strg() const --> class std::weak_ptr<class binpac::IMemoryStorage>");
	}
}


// File: unknown/unknown_5.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // Storage
#include <../plugins/plugin.h> // Storage::get_list
#include <../plugins/plugin.h> // Storage::info
#include <../plugins/plugin.h> // Storage::mem_can_handle
#include <../plugins/plugin.h> // Storage::mem_close
#include <../plugins/plugin.h> // Storage::mem_get
#include <../plugins/plugin.h> // Storage::mem_open
#include <../plugins/plugin.h> // Storage::mem_put
#include <../plugins/plugin.h> // Storage::operator=
#include <../plugins/plugin.h> // StorageInfo
#include <../plugins/plugin.h> // StorageState
#include <../plugins/plugin.h> // StorageState::StorageState
#include <../plugins/plugin.h> // UUL_Entry
#include <../plugins/plugin.h> // UUL_Entry::UUL_Entry
#include <initializer_list> // std::initializer_list
#include <initializer_list> // std::initializer_list<char>::begin
#include <initializer_list> // std::initializer_list<char>::end
#include <initializer_list> // std::initializer_list<char>::initializer_list
#include <initializer_list> // std::initializer_list<char>::size
#include <memory> // std::allocator
#include <memory> // std::allocator<char>::address
#include <memory> // std::allocator<char>::allocate
#include <memory> // std::allocator<char>::allocator
#include <memory> // std::allocator<char>::deallocate
#include <memory> // std::allocator<char>::max_size
#include <memory> // std::allocator<unsigned int>::address
#include <memory> // std::allocator<unsigned int>::allocate
#include <memory> // std::allocator<unsigned int>::allocator
#include <memory> // std::allocator<unsigned int>::deallocate
#include <memory> // std::allocator<unsigned int>::max_size
#include <memory> // std::shared_ptr
#include <memory> // std::shared_ptr<DirectMemory>::get
#include <memory> // std::shared_ptr<DirectMemory>::operator*
#include <memory> // std::shared_ptr<DirectMemory>::operator=
#include <memory> // std::shared_ptr<DirectMemory>::reset
#include <memory> // std::shared_ptr<DirectMemory>::shared_ptr
#include <memory> // std::shared_ptr<DirectMemory>::swap
#include <memory> // std::shared_ptr<DirectMemory>::unique
#include <memory> // std::shared_ptr<MemoryRegion>::get
#include <memory> // std::shared_ptr<MemoryRegion>::operator*
#include <memory> // std::shared_ptr<MemoryRegion>::operator=
#include <memory> // std::shared_ptr<MemoryRegion>::reset
#include <memory> // std::shared_ptr<MemoryRegion>::shared_ptr
#include <memory> // std::shared_ptr<MemoryRegion>::swap
#include <memory> // std::shared_ptr<MemoryRegion>::unique
#include <memory> // std::shared_ptr<StorageState>::get
#include <memory> // std::shared_ptr<StorageState>::operator*
#include <memory> // std::shared_ptr<StorageState>::operator=
#include <memory> // std::shared_ptr<StorageState>::reset
#include <memory> // std::shared_ptr<StorageState>::shared_ptr
#include <memory> // std::shared_ptr<StorageState>::swap
#include <memory> // std::shared_ptr<StorageState>::unique
#include <memory> // std::weak_ptr
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::expired
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::lock
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::operator=
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::reset
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::swap
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::weak_ptr
#include <set> // std::set
#include <sstream> // __str__
#include <string> // std::basic_string
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Chassign
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Check_offset
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Check_offset_exclusive
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Clamp_suffix_size
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Construct
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Copy
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Copy_s
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Eos
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Grow
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Inside
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Swap_bx
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Tidy
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Traits_compare
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Xlen
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Xran
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::append
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::assign
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::at
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::back
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::basic_string
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::begin
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::c_str
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::capacity
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::cbegin
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::cend
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::clear
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::compare
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::copy
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::data
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::empty
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::end
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::erase
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find_first_not_of
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find_first_of
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find_last_not_of
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find_last_of
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::front
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::get_allocator
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::insert
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::length
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::max_size
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::operator+=
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::operator=
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::operator[]
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::pop_back
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::push_back
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::replace
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::reserve
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::resize
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::rfind
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::shrink_to_fit
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::size
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::substr
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::swap
#include <string> // std::char_traits
#include <string> // std::char_traits<char>::_Copy_s
#include <string> // std::char_traits<char>::assign
#include <string> // std::char_traits<char>::compare
#include <string> // std::char_traits<char>::copy
#include <string> // std::char_traits<char>::eof
#include <string> // std::char_traits<char>::eq
#include <string> // std::char_traits<char>::eq_int_type
#include <string> // std::char_traits<char>::find
#include <string> // std::char_traits<char>::length
#include <string> // std::char_traits<char>::lt
#include <string> // std::char_traits<char>::move
#include <string> // std::char_traits<char>::not_eof
#include <string> // std::char_traits<char>::to_char_type
#include <string> // std::char_traits<char>::to_int_type
#include <vector> // std::_Vector_const_iterator
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::_Compat
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::_Rechecked
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::_Unchecked
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::_Vector_const_iterator
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator!=
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator*
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator+
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator++
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator+=
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator-
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator--
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator-=
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator==
#include <vector> // std::_Vector_const_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator[]
#include <vector> // std::_Vector_iterator
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::_Rechecked
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::_Unchecked
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::_Vector_iterator
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator*
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator+
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator++
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator+=
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator-
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator--
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator-=
#include <vector> // std::_Vector_iterator<std::_Vector_val<std::_Simple_types<unsigned int> > >::operator[]
#include <vector> // std::_Vector_val
#include <vector> // std::vector
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::_Construct_n
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::_Has_unused_capacity
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::_Make_iter
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::_Pop_back_n
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::_Unused_capacity
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::assign
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::at
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::back
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::begin
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::capacity
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::cbegin
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::cend
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::clear
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::data
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::empty
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::end
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::erase
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::front
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::get_allocator
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::insert
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::max_size
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::operator=
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::operator[]
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::pop_back
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::push_back
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::reserve
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::resize
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::shrink_to_fit
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::size
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::swap
#include <vector> // std::vector<unsigned int, std::allocator<unsigned int> >::vector
#include <xmemory0> // std::_Simple_types
#include <xstddef> // std::less
#include <xstring> // std::_String_const_iterator
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::_Compat
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::_Rechecked
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::_String_const_iterator
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::_Unchecked
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator!=
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator*
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator+
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator++
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator+=
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator-
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator--
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator-=
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator==
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator[]
#include <xstring> // std::_String_val
#include <xutility> // std::_Container_base0
#include <xutility> // std::random_access_iterator_tag
#include <xutility> // std::random_access_iterator_tag::random_access_iterator_tag

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

// MemoryRegion file: line:6
struct PyCallBack_MemoryRegion : public MemoryRegion {
	using MemoryRegion::MemoryRegion;

	unsigned int GetSize() override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const MemoryRegion *>(this), "GetSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>();
			if (pybind11::detail::cast_is_temporary_value_reference<unsigned int>::value) {
				static pybind11::detail::overload_caster_t<unsigned int> caster;
				return pybind11::detail::cast_ref<unsigned int>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<unsigned int>(std::move(o));
		}
		return MemoryRegion::GetSize();
	}
	void SetSize(unsigned int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const MemoryRegion *>(this), "SetSize");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return MemoryRegion::SetSize(a0);
	}
	void SetSizeInternal(unsigned int a0) override { 
		pybind11::gil_scoped_acquire gil;
		pybind11::function overload = pybind11::get_overload(static_cast<const MemoryRegion *>(this), "SetSizeInternal");
		if (overload) {
			auto o = overload.operator()<pybind11::return_value_policy::reference>(a0);
			if (pybind11::detail::cast_is_temporary_value_reference<void>::value) {
				static pybind11::detail::overload_caster_t<void> caster;
				return pybind11::detail::cast_ref<void>(std::move(o), caster);
			}
			else return pybind11::detail::cast_safe<void>(std::move(o));
		}
		return MemoryRegion::SetSizeInternal(a0);
	}
};

void bind_unknown_unknown_5(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // MemoryRegion file: line:6
		pybind11::class_<MemoryRegion, std::shared_ptr<MemoryRegion>, PyCallBack_MemoryRegion> cl(M(""), "MemoryRegion", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<class binpac::offset_ptr, class binpac::offset_ptr>(), pybind11::arg("begin_"), pybind11::arg("end_"));

		cl.def(pybind11::init<PyCallBack_MemoryRegion const &>());
		cl.def_readwrite("name", &MemoryRegion::name);
		cl.def_readwrite("begin", &MemoryRegion::begin);
		cl.def_readwrite("end", &MemoryRegion::end);
		cl.def_readwrite("parent", &MemoryRegion::parent);
		cl.def_readwrite("dirty", &MemoryRegion::dirty);
		cl.def("isDirty", (bool (MemoryRegion::*)()) &MemoryRegion::isDirty, "C++: MemoryRegion::isDirty() --> bool");
		cl.def("GetSize", (unsigned int (MemoryRegion::*)()) &MemoryRegion::GetSize, "C++: MemoryRegion::GetSize() --> unsigned int");
		cl.def("SetSize", (void (MemoryRegion::*)(unsigned int)) &MemoryRegion::SetSize, "C++: MemoryRegion::SetSize(unsigned int) --> void", pybind11::arg("size"));
		cl.def("SetSizeInternal", (void (MemoryRegion::*)(unsigned int)) &MemoryRegion::SetSizeInternal, "C++: MemoryRegion::SetSizeInternal(unsigned int) --> void", pybind11::arg("size"));
		cl.def("assign", (class MemoryRegion & (MemoryRegion::*)(const class MemoryRegion &)) &MemoryRegion::operator=, "C++: MemoryRegion::operator=(const class MemoryRegion &) --> class MemoryRegion &", pybind11::return_value_policy::automatic, pybind11::arg(""));
	}
	{ // UUL_Entry file:../plugins/plugin.h line:18
		pybind11::class_<UUL_Entry, std::shared_ptr<UUL_Entry>> cl(M(""), "UUL_Entry", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());

		cl.def(pybind11::init<const struct UUL_Entry &>(), pybind11::arg("e2"));

		cl.def_readwrite("id", &UUL_Entry::id);
		cl.def_readwrite("type", &UUL_Entry::type);
		cl.def_readwrite("size", &UUL_Entry::size);
		cl.def_readwrite("name", &UUL_Entry::name);
		cl.def_readwrite("mem", &UUL_Entry::mem);
		cl.def_readwrite("plugin_state", &UUL_Entry::plugin_state);
	}
}


// File: unity_unpacker_lib.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // UUL_Entry
#include <../plugins/plugin.h> // UUL_Entry::UUL_Entry
#include <initializer_list> // std::initializer_list
#include <initializer_list> // std::initializer_list<char>::begin
#include <initializer_list> // std::initializer_list<char>::end
#include <initializer_list> // std::initializer_list<char>::initializer_list
#include <initializer_list> // std::initializer_list<char>::size
#include <memory> // std::allocator
#include <memory> // std::allocator<char>::address
#include <memory> // std::allocator<char>::allocate
#include <memory> // std::allocator<char>::allocator
#include <memory> // std::allocator<char>::deallocate
#include <memory> // std::allocator<char>::max_size
#include <memory> // std::weak_ptr
#include <set> // std::set
#include <sstream> // __str__
#include <string> // std::basic_string
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Chassign
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Check_offset
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Check_offset_exclusive
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Clamp_suffix_size
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Construct
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Copy
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Copy_s
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Eos
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Grow
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Inside
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Swap_bx
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Tidy
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Traits_compare
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Xlen
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Xran
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::append
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::assign
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::at
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::back
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::basic_string
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::begin
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::c_str
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::capacity
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::cbegin
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::cend
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::clear
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::compare
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::copy
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::data
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::empty
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::end
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::erase
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find_first_not_of
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find_first_of
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find_last_not_of
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::find_last_of
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::front
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::get_allocator
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::insert
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::length
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::max_size
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::operator+=
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::operator=
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::operator[]
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::pop_back
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::push_back
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::replace
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::reserve
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::resize
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::rfind
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::shrink_to_fit
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::size
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::substr
#include <string> // std::basic_string<char, std::char_traits<char>, std::allocator<char> >::swap
#include <string> // std::char_traits
#include <string> // std::char_traits<char>::_Copy_s
#include <string> // std::char_traits<char>::assign
#include <string> // std::char_traits<char>::compare
#include <string> // std::char_traits<char>::copy
#include <string> // std::char_traits<char>::eof
#include <string> // std::char_traits<char>::eq
#include <string> // std::char_traits<char>::eq_int_type
#include <string> // std::char_traits<char>::find
#include <string> // std::char_traits<char>::length
#include <string> // std::char_traits<char>::lt
#include <string> // std::char_traits<char>::move
#include <string> // std::char_traits<char>::not_eof
#include <string> // std::char_traits<char>::to_char_type
#include <string> // std::char_traits<char>::to_int_type
#include <unity_unpacker_lib.h> // UUL_CanHandle
#include <unity_unpacker_lib.h> // UUL_Close
#include <unity_unpacker_lib.h> // UUL_Connect
#include <unity_unpacker_lib.h> // UUL_Disconnect
#include <unity_unpacker_lib.h> // UUL_ExportFile
#include <unity_unpacker_lib.h> // UUL_Get
#include <unity_unpacker_lib.h> // UUL_GetList
#include <unity_unpacker_lib.h> // UUL_ImportFile
#include <unity_unpacker_lib.h> // UUL_Initiate
#include <unity_unpacker_lib.h> // UUL_Open
#include <unity_unpacker_lib.h> // UUL_Put
#include <unity_unpacker_lib.h> // UUL_RefreshList
#include <unity_unpacker_lib.h> // UUL_State
#include <unity_unpacker_lib.h> // UUL_Terminate
#include <xmemory0> // std::_Simple_types
#include <xstddef> // std::less
#include <xstddef> // std::less<UUL_Entry>::operator()
#include <xstring> // std::_String_const_iterator
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::_Compat
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::_Rechecked
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::_String_const_iterator
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::_Unchecked
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator!=
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator*
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator+
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator++
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator+=
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator-
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator--
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator-=
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator==
#include <xstring> // std::_String_const_iterator<std::_String_val<std::_Simple_types<char> > >::operator[]
#include <xstring> // std::_String_val
#include <xutility> // std::_Container_base0
#include <xutility> // std::random_access_iterator_tag
#include <xutility> // std::random_access_iterator_tag::random_access_iterator_tag

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_unity_unpacker_lib(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // UUL_State file:unity_unpacker_lib.h line:37
		pybind11::class_<UUL_State, std::shared_ptr<UUL_State>> cl(M(""), "UUL_State", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());
		cl.def_readwrite("mmf", &UUL_State::mmf);
		cl.def_readwrite("root", &UUL_State::root);
	}
	// UUL_Open(class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >, bool) file:unity_unpacker_lib.h line:46
	M("").def("UUL_Open", (struct UUL_State * (*)(class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >, bool)) &UUL_Open, "C++: UUL_Open(class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >, bool) --> struct UUL_State *", pybind11::return_value_policy::automatic, pybind11::arg(""), pybind11::arg(""));

	// UUL_Close(struct UUL_State *) file:unity_unpacker_lib.h line:47
	M("").def("UUL_Close", (void (*)(struct UUL_State *)) &UUL_Close, "C++: UUL_Close(struct UUL_State *) --> void", pybind11::arg(""));

	// UUL_CanHandle(struct UUL_State *, unsigned int) file:unity_unpacker_lib.h line:54
	M("").def("UUL_CanHandle", (bool (*)(struct UUL_State *, unsigned int)) &UUL_CanHandle, "C++: UUL_CanHandle(struct UUL_State *, unsigned int) --> bool", pybind11::arg(""), pybind11::arg(""));

	// UUL_GetList(struct UUL_State *) file:unity_unpacker_lib.h line:57
	M("").def("UUL_GetList", (class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > * (*)(struct UUL_State *)) &UUL_GetList, "C++: UUL_GetList(struct UUL_State *) --> class tcl::tree<struct UUL_Entry, struct std::less<struct UUL_Entry> > *", pybind11::return_value_policy::automatic, pybind11::arg(""));

	// UUL_Get(struct UUL_State *, unsigned int, class MemoryRegion *) file:unity_unpacker_lib.h line:59
	M("").def("UUL_Get", (bool (*)(struct UUL_State *, unsigned int, class MemoryRegion *)) &UUL_Get, "C++: UUL_Get(struct UUL_State *, unsigned int, class MemoryRegion *) --> bool", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""));

	// UUL_Put(struct UUL_State *, unsigned int, class MemoryRegion *) file:unity_unpacker_lib.h line:61
	M("").def("UUL_Put", (bool (*)(struct UUL_State *, unsigned int, class MemoryRegion *)) &UUL_Put, "C++: UUL_Put(struct UUL_State *, unsigned int, class MemoryRegion *) --> bool", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""));

	// UUL_ExportFile(struct UUL_State *, unsigned int, class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >) file:unity_unpacker_lib.h line:63
	M("").def("UUL_ExportFile", (bool (*)(struct UUL_State *, unsigned int, class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >)) &UUL_ExportFile, "C++: UUL_ExportFile(struct UUL_State *, unsigned int, class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >) --> bool", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""));

	// UUL_ImportFile(struct UUL_State *, unsigned int, class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >) file:unity_unpacker_lib.h line:65
	M("").def("UUL_ImportFile", (bool (*)(struct UUL_State *, unsigned int, class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >)) &UUL_ImportFile, "C++: UUL_ImportFile(struct UUL_State *, unsigned int, class std::basic_string<char, struct std::char_traits<char>, class std::allocator<char> >) --> bool", pybind11::arg(""), pybind11::arg(""), pybind11::arg(""));

}


// File: std/memory_1.cpp
#include <../include/tcl/tree.h> // tcl::tree
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::insert
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::operator=
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::swap
#include <../include/tcl/tree.h> // tcl::tree<UUL_Entry, std::less<UUL_Entry> >::tree
#include <../include/tcl/tree.h> // tcl::tree_deref_less
#include <../plugins/plugin.h> // StorageState
#include <../plugins/plugin.h> // StorageState::StorageState
#include <../plugins/plugin.h> // UUL_Entry
#include <memory> // std::_Enable_shared
#include <memory> // std::allocator
#include <memory> // std::shared_ptr
#include <memory> // std::shared_ptr<DirectMemory>::get
#include <memory> // std::shared_ptr<DirectMemory>::operator*
#include <memory> // std::shared_ptr<DirectMemory>::operator=
#include <memory> // std::shared_ptr<DirectMemory>::reset
#include <memory> // std::shared_ptr<DirectMemory>::shared_ptr
#include <memory> // std::shared_ptr<DirectMemory>::swap
#include <memory> // std::shared_ptr<DirectMemory>::unique
#include <memory> // std::shared_ptr<MemoryMappedFile>::get
#include <memory> // std::shared_ptr<MemoryMappedFile>::operator*
#include <memory> // std::shared_ptr<MemoryMappedFile>::operator=
#include <memory> // std::shared_ptr<MemoryMappedFile>::reset
#include <memory> // std::shared_ptr<MemoryMappedFile>::shared_ptr
#include <memory> // std::shared_ptr<MemoryMappedFile>::swap
#include <memory> // std::shared_ptr<MemoryMappedFile>::unique
#include <memory> // std::shared_ptr<MemoryRegion>::get
#include <memory> // std::shared_ptr<MemoryRegion>::operator*
#include <memory> // std::shared_ptr<MemoryRegion>::operator=
#include <memory> // std::shared_ptr<MemoryRegion>::reset
#include <memory> // std::shared_ptr<MemoryRegion>::shared_ptr
#include <memory> // std::shared_ptr<MemoryRegion>::swap
#include <memory> // std::shared_ptr<MemoryRegion>::unique
#include <memory> // std::shared_ptr<StorageState>::get
#include <memory> // std::shared_ptr<StorageState>::operator*
#include <memory> // std::shared_ptr<StorageState>::operator=
#include <memory> // std::shared_ptr<StorageState>::reset
#include <memory> // std::shared_ptr<StorageState>::shared_ptr
#include <memory> // std::shared_ptr<StorageState>::swap
#include <memory> // std::shared_ptr<StorageState>::unique
#include <memory> // std::shared_ptr<binpac::IMemoryStorage>::get
#include <memory> // std::shared_ptr<binpac::IMemoryStorage>::operator*
#include <memory> // std::shared_ptr<binpac::IMemoryStorage>::operator=
#include <memory> // std::shared_ptr<binpac::IMemoryStorage>::reset
#include <memory> // std::shared_ptr<binpac::IMemoryStorage>::shared_ptr
#include <memory> // std::shared_ptr<binpac::IMemoryStorage>::swap
#include <memory> // std::shared_ptr<binpac::IMemoryStorage>::unique
#include <memory> // std::shared_ptr<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::get
#include <memory> // std::shared_ptr<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::operator*
#include <memory> // std::shared_ptr<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::operator=
#include <memory> // std::shared_ptr<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::reset
#include <memory> // std::shared_ptr<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::shared_ptr
#include <memory> // std::shared_ptr<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::swap
#include <memory> // std::shared_ptr<tcl::tree<UUL_Entry, std::less<UUL_Entry> > >::unique
#include <memory> // std::weak_ptr
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::expired
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::lock
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::operator=
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::reset
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::swap
#include <memory> // std::weak_ptr<binpac::IMemoryStorage>::weak_ptr
#include <set> // std::set
#include <sstream> // __str__
#include <string> // std::basic_string
#include <string> // std::char_traits
#include <xstddef> // std::less

#include <pybind11/pybind11.h>

#ifndef BINDER_PYBIND11_TYPE_CASTER
	#define BINDER_PYBIND11_TYPE_CASTER
	PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);
	PYBIND11_DECLARE_HOLDER_TYPE(T, T*);
#endif

void bind_std_memory_1(std::function< pybind11::module &(std::string const &namespace_) > &M)
{
	{ // std::weak_ptr file:memory line:1006
		pybind11::class_<std::weak_ptr<binpac::IMemoryStorage>, std::shared_ptr<std::weak_ptr<binpac::IMemoryStorage>>, std::_Ptr_base<binpac::IMemoryStorage>> cl(M("std"), "weak_ptr_binpac_IMemoryStorage_t", "");
		pybind11::handle cl_type = cl;

		cl.def(pybind11::init<>());

		cl.def(pybind11::init<const class std::weak_ptr<class binpac::IMemoryStorage> &>(), pybind11::arg("_Other"));

		cl.def("assign", (class std::weak_ptr<class binpac::IMemoryStorage> & (std::weak_ptr<binpac::IMemoryStorage>::*)(const class std::weak_ptr<class binpac::IMemoryStorage> &)) &std::weak_ptr<binpac::IMemoryStorage>::operator=, "C++: std::weak_ptr<binpac::IMemoryStorage>::operator=(const class std::weak_ptr<class binpac::IMemoryStorage> &) --> class std::weak_ptr<class binpac::IMemoryStorage> &", pybind11::return_value_policy::automatic, pybind11::arg("_Right"));
		cl.def("reset", (void (std::weak_ptr<binpac::IMemoryStorage>::*)()) &std::weak_ptr<binpac::IMemoryStorage>::reset, "C++: std::weak_ptr<binpac::IMemoryStorage>::reset() --> void");
		cl.def("swap", (void (std::weak_ptr<binpac::IMemoryStorage>::*)(class std::weak_ptr<class binpac::IMemoryStorage> &)) &std::weak_ptr<binpac::IMemoryStorage>::swap, "C++: std::weak_ptr<binpac::IMemoryStorage>::swap(class std::weak_ptr<class binpac::IMemoryStorage> &) --> void", pybind11::arg("_Other"));
		cl.def("expired", (bool (std::weak_ptr<binpac::IMemoryStorage>::*)() const) &std::weak_ptr<binpac::IMemoryStorage>::expired, "C++: std::weak_ptr<binpac::IMemoryStorage>::expired() const --> bool");
		cl.def("lock", (class std::shared_ptr<class binpac::IMemoryStorage> (std::weak_ptr<binpac::IMemoryStorage>::*)() const) &std::weak_ptr<binpac::IMemoryStorage>::lock, "C++: std::weak_ptr<binpac::IMemoryStorage>::lock() const --> class std::shared_ptr<class binpac::IMemoryStorage>");
		cl.def("use_count", (long (std::_Ptr_base<binpac::IMemoryStorage>::*)() const) &std::_Ptr_base<binpac::IMemoryStorage>::use_count, "C++: std::_Ptr_base<binpac::IMemoryStorage>::use_count() const --> long");
		cl.def("_Swap", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)(class std::_Ptr_base<class binpac::IMemoryStorage> &)) &std::_Ptr_base<binpac::IMemoryStorage>::_Swap, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Swap(class std::_Ptr_base<class binpac::IMemoryStorage> &) --> void", pybind11::arg("_Right"));
		cl.def("_Get_deleter", (void * (std::_Ptr_base<binpac::IMemoryStorage>::*)(const class type_info &) const) &std::_Ptr_base<binpac::IMemoryStorage>::_Get_deleter, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Get_deleter(const class type_info &) const --> void *", pybind11::return_value_policy::automatic, pybind11::arg("_Typeid"));
		cl.def("_Get", (class binpac::IMemoryStorage * (std::_Ptr_base<binpac::IMemoryStorage>::*)() const) &std::_Ptr_base<binpac::IMemoryStorage>::_Get, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Get() const --> class binpac::IMemoryStorage *", pybind11::return_value_policy::automatic);
		cl.def("_Expired", (bool (std::_Ptr_base<binpac::IMemoryStorage>::*)() const) &std::_Ptr_base<binpac::IMemoryStorage>::_Expired, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Expired() const --> bool");
		cl.def("_Decref", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)()) &std::_Ptr_base<binpac::IMemoryStorage>::_Decref, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Decref() --> void");
		cl.def("_Reset", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)()) &std::_Ptr_base<binpac::IMemoryStorage>::_Reset, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Reset() --> void");
		cl.def("_Reset", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)(class binpac::IMemoryStorage *, class std::_Ref_count_base *)) &std::_Ptr_base<binpac::IMemoryStorage>::_Reset, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Reset(class binpac::IMemoryStorage *, class std::_Ref_count_base *) --> void", pybind11::arg("_Other_ptr"), pybind11::arg("_Other_rep"));
		cl.def("_Reset", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)(class binpac::IMemoryStorage *, class std::_Ref_count_base *, bool)) &std::_Ptr_base<binpac::IMemoryStorage>::_Reset, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Reset(class binpac::IMemoryStorage *, class std::_Ref_count_base *, bool) --> void", pybind11::arg("_Other_ptr"), pybind11::arg("_Other_rep"), pybind11::arg("_Throw"));
		cl.def("_Reset0", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)(class binpac::IMemoryStorage *, class std::_Ref_count_base *)) &std::_Ptr_base<binpac::IMemoryStorage>::_Reset0, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Reset0(class binpac::IMemoryStorage *, class std::_Ref_count_base *) --> void", pybind11::arg("_Other_ptr"), pybind11::arg("_Other_rep"));
		cl.def("_Decwref", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)()) &std::_Ptr_base<binpac::IMemoryStorage>::_Decwref, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Decwref() --> void");
		cl.def("_Resetw", (void (std::_Ptr_base<binpac::IMemoryStorage>::*)()) &std::_Ptr_base<binpac::IMemoryStorage>::_Resetw, "C++: std::_Ptr_base<binpac::IMemoryStorage>::_Resetw() --> void");
	}
}




#include <pybind11/pybind11.h>

typedef std::function< pybind11::module & (std::string const &) > ModuleGetter;

void bind_std_xutility(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_unknown_unknown(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_vcruntime_typeinfo(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_unknown_unknown_1(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_unknown_unknown_2(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_unknown_unknown_3(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind____include_tcl_tree(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_std_memory(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_unknown_unknown_4(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_unknown_unknown_5(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_unity_unpacker_lib(std::function< pybind11::module &(std::string const &namespace_) > &M);
void bind_std_memory_1(std::function< pybind11::module &(std::string const &namespace_) > &M);


PYBIND11_PLUGIN(pyunitylib) {
	std::map <std::string, std::shared_ptr<pybind11::module> > modules;
	ModuleGetter M = [&](std::string const &namespace_) -> pybind11::module & {
		auto it = modules.find(namespace_);
		if( it == modules.end() ) throw std::runtime_error("Attempt to access pybind11::module for namespace " + namespace_ + " before it was created!!!");
		return * it->second;
	};

	modules[""] = std::make_shared<pybind11::module>("pyunitylib", "pyunitylib module");

	std::vector< std::pair<std::string, std::string> > sub_modules {
		{"", "binpac"},
		{"", "std"},
		{"", "tcl"},
	};
	for(auto &p : sub_modules ) modules[p.first.size() ? p.first+"::"+p.second : p.second] = std::make_shared<pybind11::module>( modules[p.first]->def_submodule(p.second.c_str(), ("Bindings for " + p.first + "::" + p.second + " namespace").c_str() ) );

	bind_std_xutility(M);
	bind_unknown_unknown(M);
	bind_vcruntime_typeinfo(M);
	bind_unknown_unknown_1(M);
	bind_unknown_unknown_2(M);
	bind_unknown_unknown_3(M);
	bind____include_tcl_tree(M);
	bind_std_memory(M);
	bind_unknown_unknown_4(M);
	bind_unknown_unknown_5(M);
	bind_unity_unpacker_lib(M);
	bind_std_memory_1(M);

	return modules[""]->ptr();
}

// Source list file: ../src/pyunitylib.sources
// pyunitylib.cpp
// std/xutility.cpp
// unknown/unknown.cpp
// vcruntime_typeinfo.cpp
// unknown/unknown_1.cpp
// unknown/unknown_2.cpp
// unknown/unknown_3.cpp
// __/include/tcl/tree.cpp
// std/memory.cpp
// unknown/unknown_4.cpp
// unknown/unknown_5.cpp
// unity_unpacker_lib.cpp
// std/memory_1.cpp

// Modules list file: ../src/pyunitylib.modules
// binpac std tcl 
